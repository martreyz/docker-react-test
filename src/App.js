import './App.css';

function App() {
  return (
    <div className="App">
      <h1 className="App_title">Hi World!</h1>
      <p className="App_text">This is my first dockerized React JS App :)</p>
    </div>
  );
}

export default App;
